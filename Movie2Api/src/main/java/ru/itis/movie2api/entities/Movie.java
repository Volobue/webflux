package ru.itis.movie2api.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class Movie {
    private String name;
    private String author;
    private String description;
}
