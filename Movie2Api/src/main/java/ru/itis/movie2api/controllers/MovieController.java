package ru.itis.movie2api.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.movie2api.entities.Movie;

import java.util.List;

@RestController
@RequestMapping("/books")
public class MovieController {

    @GetMapping()
    public List<Movie> getAllMovies() throws InterruptedException {
        Thread.sleep(5000);
        return List.of(
                Movie.builder()
                        .name("First")
                        .author("Firstov")
                        .description("First first first first first")
                        .build(),
                Movie.builder()
                        .name("Second")
                        .author("Secondov")
                        .description("Second second second")
                        .build(),
                Movie.builder()
                        .name("Three")
                        .author("Threefov")
                        .description("Three three three")
                        .build()
        );
    }
}
