package ru.itis.movie2api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Movie2ApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(Movie2ApiApplication.class, args);
    }

}
