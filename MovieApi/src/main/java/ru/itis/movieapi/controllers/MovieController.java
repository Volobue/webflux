package ru.itis.movieapi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.movieapi.entities.Movie;

import java.util.List;

@RestController
@RequestMapping("/movies")
public class MovieController {

    @GetMapping()
    public List<Movie> getAllMovies() {
        return List.of(
                Movie.builder()
                        .name("First")
                        .author("Firstov")
                        .description("First first first first first")
                        .build(),
                Movie.builder()
                        .name("Second")
                        .author("Secondov")
                        .description("Second second second")
                        .build(),
                Movie.builder()
                        .name("Three")
                        .author("Threefov")
                        .description("Three three three")
                        .build()
        );
    }
}
