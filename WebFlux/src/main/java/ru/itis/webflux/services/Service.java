package ru.itis.webflux.services;

import reactor.core.publisher.Flux;
import ru.itis.webflux.entities.Movie;

public interface Service {
    Flux<Movie> getMovies();
}
