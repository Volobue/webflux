package ru.itis.webflux.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import ru.itis.webflux.clients.Client;
import ru.itis.webflux.entities.Movie;

import java.util.List;

@Component
@AllArgsConstructor
public class MovieServiceImpl implements Service {

    private final List<Client> clients;

    @Override
    public Flux<Movie> getMovies() {
        List<Flux<Movie>> fluxes = clients.stream().map(this::getMovies).toList();
        return Flux.merge((fluxes));
    }

    private Flux<Movie> getMovies(Client client) {
        return client.getMovies()
                .subscribeOn(Schedulers.boundedElastic());
    }

}
