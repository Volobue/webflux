package ru.itis.webflux.clients;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import ru.itis.webflux.entities.Movie;

import java.util.Arrays;

@Component
public class MovieClientImpl implements Client {
    private final WebClient client;

    public MovieClientImpl(@Value("${movie.api.url}") String url) {
        client = WebClient.builder()
                .baseUrl(url)
                .build();
    }

    @Override
    public Flux<Movie> getMovies(){
        return client.get()
                .accept(MediaType.APPLICATION_JSON)
                .exchangeToFlux(clientResponse -> clientResponse.bodyToFlux(Movie[].class))
                .flatMapIterable(Arrays::asList);
    }
}

