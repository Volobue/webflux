package ru.itis.webflux.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;

@AllArgsConstructor
@Builder
public class Movie {
    private String name;
    private String author;
    private String description;
}